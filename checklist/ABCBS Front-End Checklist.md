# ABCBS Front-End Checklist

## Stylesheet testing

-- view pages using test stylesheet (chrome instructions for running the stylesheet from local files)

<br>

---

<br>

## Accessibility

### Common issues

- Aria-labelledby and ID attributes that are typo'd or missing
- Input and label pairs with missing/mismatched for and id attributes
- Missing titles on iframes
- Color contrast not meeting 4.5
- Small font-size - While there isn't a specific minimum font-size we want to try and stick to:
  - 18px or larger for body copy
  - 16px for disclaimers

### Finding issues

- Chrome and Firefox have built in accessibility testers
- AXE dev tools extension for Firefox and Chrome
- AXE dev tools also have a VSCode extension to find HTML errors while coding

### Fixing issues

- https://www.a11yproject.com/checklist/

#### Resources on how to fix issues

- WCAG 2.0/2.1 Guidelines (https://www.w3.org/WAI/standards-guidelines/wcag/)
  - This is the primary standards and documentation for meeting WCAG 2.0
- A11y project (https://www.a11yproject.com/)
  - This site has resources for how to code your website to be more accessible and how to fix common issues

#### Color contrast

- Firefox and Chrome have built in color pickers that show the current ratio
- WebAim also has a tool to input your foreground and background colors and then check and tweak the colors until they meet the 4.5 rating (https://webaim.org/resources/contrastchecker/)

<br>

---

<br>

## Browser testing

- IE 11 (This may be dropped for 2022)
- Firefox
- Firefox (Android)
- Chrome
- Chrome (Android)
- Safari (iOS)
- Safari (Mac OS)
- Edge (This is basically Chrome)