# JS Style Guide

## Spacing

- Use two spaces to indent
- Use one space between functions

---

## Comments

There are three levels of comments (File, Section, and Sub Section)

### File

```JS
// ---------------------------------------------
// ======---------------------------------======
// File_Name
// ======---------------------------------======
// ---------------------------------------------
```

### Section

```JS
// ====---------------====
// Section_Name
// ====---------------====
```

### Subsection

```JS
// ===------ Subsection_Name ------===
```

### Note

Notes should be used as a temporary note during dev. If you need to note how you are going to do something or come back to later.

```JS
// NOTE: -------------====
// Note goes here and wraps to the next line
// if it gets long.
// ====---------------====
```