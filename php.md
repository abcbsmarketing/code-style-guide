# PHP Style Guide

## Spacing

Use two spaces to indent

```PHP
<div>
  <h1>Hello World</h1>
</div>
```

<br>

---

<br>

## Variable naming

Variable names should be camel case this is generally how most people write variable names for PHP

```PHP
$variableName = 'tacos';
```

<br>

---

<br>

## IF statements

There are two ways to write if statements.

### If statements using curly braces

This is great for comparing and setting values. I generally avoid this for loop / when html is needed

```PHP
<?php
  if($i === 1) {
    ...
  } elseif ($i === 2) {
    ...
  } else {
    ...
  }
?>
```

### If statements using semicolons / colons / endif;

This is my preference for working with the WordPress loop or when HTML is involved. This is a bit cleaner and easier to read for after large blocks of HTML.

```PHP
<?php if(i === 1) : ?>
  <h1 class="page-title--mod1"><?php the_title(); ?></h1>
<?php elseif(i === 2) : ?>
  <h1 class="page-title--mod2"><?php the_title(); ?></h1>
<?php else : ?>
  <h1 class="page-title"><?php the_title(); ?></h1>
<?php endif; ?>
```

<br>

---

<br>

## For / foreach loops

Same logic for if statements above for setting variables or other things that isn't looping and outputting HTML use `{}` curly braces otherwise use the semicolon endfor version

### Curly braces

```PHP
for ($i = 1; $i <= 10; $i++) {
  ...
}

foreach($arr as  $value) {
  ...
}
```

### semicolon / end

``` PHP
<?php for($i = 1; $i <= 10; $i++) : ?>
  <li><?php echo $i; ?>
<?php endfor; ?>

<?php for($attr as $value) : ?>
  <li><?php echo $value; ?>
<?php endfor; ?>
```

<br>

---

<br>

## while loop

This will primarily be used when working with the WordPress loop

Same logic for if statements above for setting variables or other things that isn't looping and outputting HTML use `{}` curly braces otherwise use the semicolon end version.

### Curly braces

```PHP
while ($i < 10) {
  ...
}
```

### semicolon / end

``` PHP
<?php while ($i < 10) : ?>
  <p><?php echo $i; ?></p>
<?php endwhile; ?>
```

<br>

---

<br>

## WordPress Loops

### The basic / default WordPress Loop

```PHP
<?php while (have_posts()) : the_post(); ?>
  <article>
    <h1><?php the_title(); ?></h1>
    <p><?php the_content(); ?></p>
  </article>
<?php endwhile; ?>
```

### Custom loop

``` PHP
<?php
  $customArgs = array(
    'category_name' => 'category_name', // category name
    'posts_per_page' => -1 // this makes it pull all posts
  );

  $customQuery = new WP_Query($customArgs);

  while ($customQuery->hav_posts()) : $customQuery->the_post();
?>
  <article>
    <h1><?php the_title(); ?></h1>
    <p><?php the_content(); ?></p>
  </article>
<?php endwhile; ?>
```

<br>

---

<br>

## Advanced Custom Fields (ACF)

### Basic field call

If you are calling a basic field like text field and are only using it once in an area then you can use

```PHP
<?php the_field(); ?>
```

### Image field call

Images can be a bit tricky as there are a lot of variables to them

```PHP
<?php
  $img = get_field('img');

  // check if image exists
  if($img) :
    $imgSize = 'thumb'; // this can be any image size registered with WP

    $imgUrl = $img['sizes'][$imgSize];
    $imgAlt = $img['alt'];
?>
  <img src="<?php echo esc_url($imgUrl); ?>" alt="<?php echo esc_attr($imgAlt); ?>">
<?php endif; ?>
```

### Group field call

```PHP
<?php
  $callout = get_field('callout');

  if($callout) :
    $title = $callout['title']; // this is a basic text field
    $content = $callout['content']; // this is a content field like the WP WYSIWYG
    $link = $callout['link']; // this is a link field, it can be used a lot like the img field
    $linkUrl = $link['url'];
    $linkTarget = 2$link['target'] ? $link['target'] : '_self';;
    $linkAlt = $link['alt'];
    $linkTitle = $link['title'];
?>

  <h2><?php echo $title; ?></h2>
  <?php echo $content; ?>
  <a herf="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo linkTitle; ?></a>

<?php endif; ?>
```
