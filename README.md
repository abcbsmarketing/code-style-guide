# ABCBS Front-End Code Style Guide

## Required Software

- [git](https://git-scm.com/)
- [Node/Npm](https://nodejs.org/en/)
- [Gulp](https://gulpjs.com/)
- [Browsersync](https://browsersync.io/)
- Prettier - [VS Code Extension](https://github.com/prettier/prettier-vscode)

## Style Guide List

- [HTML Style Guide](./html.md)
- [PHP Style Guide](./php.md)
- [SCSS Style Guide](./scss.md)
- [JS Style Guide](./js.md)
- [Git Style Guide](./git.md)