# SCSS Style Guide

## Spacing

- Use two spaces to indent
- Use one space between style blocks

```SCSS
.style {
  color: red;
}

.style--alt {
  color: blue;
}
```

---

## Variables

### Naming

Variable names should be all lowercase, no hyphens/underscores, and clearly describe what they are.

#### Colors

- For colors start with your new color and then add modifiers for it after.
- Use only variables for colors throughout the project

```SCSS
$grey: #999;
$greylight: #ddd;
$greymedium: #666;
$greydark: #333;
```

#### Fonts

For fonts prefix with the word font, then describe it's role.

```SCSS
$fontbase: 'Roboto', sans serif;
$fontcond: 'Roboto Condensed', sans serif;
```

---

## Selectors

### BEM

Selector names should follow (loosely) the BEM style of naming

`block__element--modifier`

- only use one level of block__element
- place modifiers after the block/element that it is a modifier of

#### Commenting BEM

- use a section comment in the CSS to note a new block
- use a subsection comment for elements

#### BEM example

```SCSS

// ====---------------====
// Block
// ====---------------====
.section {
  width: 100%;

  background: cyan;
}

// modifier
.section--red {
  background: red;
}

// ===------ Element ------===
.section__inner {
  width: 95%;
  max-width: 1100px;

  margin: 0 auto;
}
```

### Atoms

There are also atoms for small reusable helper class (background, colors, sizes, spacing)

Atoms should use the CSS property's first initial's of it's name and then the value, so margin-top would be mt--10

```SCSS
.mt--10 {
  margin-top: 1rem;
}

.mt--20 {
  margin-top: 2rem;
}
```

---

## Nesting

- When styling try and keep styles nested together
- Nested styles should try to be 5 selectors deep max
- leave no empty style blocks
- put modifiers last in the style block

```SCSS
.nav {
  ul {
    li {
      a {
        color: red;
      }

      // place modifiers at end
      &:first-child {
        a {
          color: blue;
        }
      }
    }
  }
}
```

---

## Comments

There are three levels of comments (File, Section, and Sub Section)

### File

```SCSS
// ---------------------------------------------
// ======---------------------------------======
// File_Name
// ======---------------------------------======
// ---------------------------------------------

.style {
  color: red;
}
```

### Section

```SCSS
// ====---------------====
// Section_Name
// ====---------------====

.style {
  color: red;
}
```

### Subsection

```SCSS
// ===------ Subsection_Name ------===

.style {
  color: red;
}
```

### Note

Notes should be used as a temporary note during dev. If you need to note how you are going to do something or come back to later.

```SCSS
// NOTE: -------------====
// Note goes here and wraps to the next line
// if it gets long.
// ====---------------====

.style {
  color: red;
}
```