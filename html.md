# HTML / Nunjucks Style Guide

## Spacing

Use two spaces to indent

```HTML
<div>
  <h1>Hello World</h1>
</div>
```

---

## Comments

- Avoid HTML comments use nunjucks comments instead
- comment the start of the section you are describing

```HTML
{# Comment example #}
<div>
  <h1>Hello World</h1>
</div>
```
