# Git Style Guide

## Branch Naming

In general keep branch names as short as possible while being descriptive. Use underscores for spaces and prefix it with the type of work it is.

### Bugfix

`bugfix_fixtitle`

### Feature

`feature_featuretitle`

### Release/Version Update

`release_releasetitle`

---

## Starting new work in git

Try to not work directly in the master branch instead make a new branch using the types above.

### Steps

1. Pull the master to check that you are up to date
2. Create a new branch `git branch bugfix_button_sizing`
3. Checkout new branch `git checkout bugfix_button_sizing`
4. do your work
5. once finished going your work push the new branch to the remote
6. Go into bitbucket and do a pull request

---

## Commits

- Try to avoid working on different tasks in one commit.
- Make commit message specific to what you are doing.
- Keep commit messages to a single sentence. If a commit message needs more than one sentence then it should probably be split into multiple messages.